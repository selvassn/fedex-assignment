# FedExAssignment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli).

Run the `npm run start-client` and `npm run start-server` in the two different terminal simultaneously to see th e application running.

In this project node is acting as a proxy layer to connect to the backend system.

## Steps to run the application

- Clone the repository
- Open terminal and run command `npm i`
- Run `npm run start-client` command to start the angular app
- Run `npm run start-server` command to start the node app
- Open the browser and navigate to `http://localhost:4200/`

## Libraries Used
- ngx-toastr 
- Bootstrap

## Exceptions - Out of Scope / Improvement

- HttpInterceptor interface can be implemented to intercept the http calls to set the headers and handle errors

## Development client server

Run `npm run start-client` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Development node server

Run `npm run start-server` for a node server. It runs in the port `http://localhost:9890/`

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).
