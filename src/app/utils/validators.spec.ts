
import { ValidatePasswordWithName } from './validators';
import { FormControl, FormGroup } from '@angular/forms';

describe('Validators', () => {
    let formGroup: FormGroup;
    beforeEach(() => {
        formGroup = new FormGroup({
            password: new FormControl('asbasbasb'),
            firstName: new FormControl(''),
            lastName: new FormControl('')
        });
    });

    it('should validate password and return null', () => {
        expect(ValidatePasswordWithName(formGroup.get('password'))).toBeNull();
    });

    it('should validate password and return true', () => {
        formGroup.get('firstName').setValue('asbasbasb');
        formGroup.get('lastName').setValue('asbasbasb');
        formGroup.get('password').markAsDirty();
        expect(ValidatePasswordWithName(formGroup.get('password'))).toBeTruthy();
    });
});

