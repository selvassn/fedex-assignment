import { AbstractControl } from '@angular/forms';

/**
 * Function to check whether password contains firstname or lastname
 */
export function ValidatePasswordWithName(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.dirty) {
        const firstName = control.parent.get('firstName').value;
        const lastName = control.parent.get('lastName').value;
        if (control.value.includes(firstName) || control.value.includes(lastName)) {
            return { passwordNameValidity : true };
        }
    }
    return null;
}
