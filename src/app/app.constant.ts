export class AppConstant {
    static EMAIL_REGEX = '^[A-Za-z0-9._]+@[A-Za-z0-9]+\.[A-Za-z]{2,4}$';
    static PASSWORD_CASE_REGEX = '^([a-z]+)([A-Z]+)|([A-Z]+([a-z]+))$';
}
