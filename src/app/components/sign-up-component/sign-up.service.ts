import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ISignUpUser } from './isign-up.interface';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SignupService {
  constructor(private http: HttpClient) { }

  /**
   * function to store user details in database
   * @param data - user information from signup form
   */
  public createUser(data: ISignUpUser): Observable<any> {
    return this.http.post(`${environment.apiUrl}/api/postUsers`, data)
      .pipe(catchError((error: any) => observableThrowError(error)));
  }
}
