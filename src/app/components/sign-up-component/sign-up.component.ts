import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ValidatePasswordWithName } from '../../utils/validators';
import { AppConstant } from '../../app.constant';
import { SignupService } from './sign-up.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-sign-up-component',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  public signUpForm: FormGroup;
  public isSubmitted = false;
  public isLoading = false;

  constructor(private formBuilder: FormBuilder,
              private toast: ToastrService,
              private signUpService: SignupService) { }

  /**
   * Initializing the form in ngOnInit
   */
  ngOnInit() {
    this.signUpForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(AppConstant.EMAIL_REGEX)]],
      password: ['', [Validators.required,
      Validators.minLength(8),
      Validators.pattern(AppConstant.PASSWORD_CASE_REGEX),
        ValidatePasswordWithName]]
    });
  }
  /**
   * Getters of controls
   * Its a small form hence all the controls were retrived separately. If form is big we can use
   * this.signUpForm.controls in getter function
   */
  get firstNameCtrl() {
    return this.signUpForm.get('firstName') as FormControl;
  }

  get lastNameCtrl() {
    return this.signUpForm.get('lastName') as FormControl;
  }

  get emailCtrl() {
    return this.signUpForm.get('email') as FormControl;
  }

  get passwordCtrl() {
    return this.signUpForm.get('password') as FormControl;
  }

  /**
   * This method will be called while form submit
   */
  onSubmit() {
    this.isSubmitted = true;
    if (this.signUpForm.invalid) {
      return;
    } else {
      this.isLoading = true;
      this.signUpService.createUser(this.signUpForm.value)
        .pipe(finalize(() => {
          this.isLoading = false;
        }))
        .subscribe((data) => {
          this.toast.success('User added successfully', 'Success', { progressBar: true });
          this.signUpForm.reset();
          Object.keys(this.signUpForm.controls).forEach(key => {
            this.signUpForm.get(key).setErrors(null);
          });
        }, error => {
          this.toast.success('User added successfully', 'Error', { progressBar: true });
        });
    }
  }
}
