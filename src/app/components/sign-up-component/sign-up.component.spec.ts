import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { SignUpComponent } from './sign-up.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SignupService } from './sign-up.service';

describe('SignUpComponent', () => {
  let component: SignUpComponent;
  let fixture: ComponentFixture<SignUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule, HttpClientTestingModule, ToastrModule.forRoot()],
      declarations: [ SignUpComponent ],
      providers: [SignupService, ToastrService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('form should be invalid for empty values in required fields', () => {
    component.firstNameCtrl.setValue('');
    component.lastNameCtrl.setValue('');
    component.emailCtrl.setValue('');
    component.passwordCtrl.setValue('');
    expect(component.signUpForm.valid).toBeFalsy();
  });

  it('form should be invalid for wrong email address in email field', () => {
    component.emailCtrl.setValue('asdasd@ddd');
    expect(component.signUpForm.valid).toBeFalsy();
  });

  it('form should be invalid for less than 8 chars in password field', () => {
    component.passwordCtrl.setValue('asdasd');
    expect(component.signUpForm.valid).toBeFalsy();
  });

  it('form should be invalid for no Uppercase chars in password field', () => {
    component.passwordCtrl.setValue('asdasd');
    expect(component.signUpForm.valid).toBeFalsy();
  });

  it('form should be invalid for password which contain first or last name in password field', () => {
    component.firstNameCtrl.setValue('asd');
    component.lastNameCtrl.setValue('uuuu');
    component.passwordCtrl.setValue('asdasd');
    expect(component.signUpForm.valid).toBeFalsy();
  });

  it('form should be invalid for empty values in required fields', () => {
    component.firstNameCtrl.setValue('');
    component.lastNameCtrl.setValue('');
    component.emailCtrl.setValue('');
    component.passwordCtrl.setValue('');
    expect(component.signUpForm.valid).toBeFalsy();
  });

  it('form should be valid', () => {
    component.firstNameCtrl.setValue('aaaaaaaaaa');
    component.lastNameCtrl.setValue('bbbbbbbbbb');
    component.emailCtrl.setValue('test@123.com');
    component.passwordCtrl.setValue('Khjhjhjh');
    expect(component.signUpForm.valid).toBeTruthy();
  });

  it('should call onsubmit method', () => {
    fixture.detectChanges();
    spyOn(component, 'onSubmit');
    const btn = fixture.debugElement.query(By.css('button')).nativeElement;
    btn.click();
    expect(component.onSubmit).toHaveBeenCalled();
  });

});
