import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ISignUpUser } from './isign-up.interface';
import { environment } from '../../../environments/environment';
import { SignupService } from './sign-up.service';

describe('SignupService', () => {
  let injector: TestBed;
  let service: SignupService;
  let httpMock: HttpTestingController;
  let reqParams: ISignUpUser;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SignupService]
    });
    injector = getTestBed();
    service = injector.get(SignupService);
    httpMock = injector.get(HttpTestingController);

    reqParams = {
      firstName: 'onnnnnnnnn',
      lastName: 'lllllllllll',
      email: 'test@123.com',
      password: 'jsfhsfJJyuu'
    };
  });

  it('signup service should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return an Observable', () => {
    service.createUser(reqParams)
    .subscribe(() => {}, err => {});
    const req = httpMock.expectOne(`${environment.apiUrl}/api/postUsers`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual(reqParams);
    req.flush({success: true});
  });

  it('should throw an error ', () => {
    service.createUser(reqParams)
      .subscribe(() => {}, err => {
        expect(err).toBe(`No supported url.`);
      });

    httpMock.expectNone(`${environment.apiUrl}/api/postUsersg`);
  });

});
