export interface ISignUpUser {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
}
