const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
var cors = require('cors');

const app = express();
const port = 9890;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const apiUrl = 'https://demo-api.now.sh/users';

app.post('/api/postUsers', (req, res) => {

    const apiPromise = new Promise((resolve, reject) => {
         const requestData =  req;
           axios.post(apiUrl, requestData)
          .then(function (response) {
              resolve(response);
          })
          .catch(function (error) {
            reject(error);
          });
    })
    
    apiPromise.then((data) => {
        res.status(200);
        res.send({ "success": true });
    }).catch((err) => {
        res.status(400);
        res.send(err);
    })
});

app.listen(port, () => console.log(`Node server is istening on port ${port}`));